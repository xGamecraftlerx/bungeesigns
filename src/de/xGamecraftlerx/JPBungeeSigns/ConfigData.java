package de.xGamecraftlerx.JPBungeeSigns;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.xGamecraftlerx.JPBungeeSigns.Utils.BungeeSign;
import de.xGamecraftlerx.JPBungeeSigns.Utils.LocationSerialiser;
import de.xGamecraftlerx.JPBungeeSigns.Utils.ServerInfo;

public class ConfigData {
	private BungeeSigns plugin;
	private boolean log;
	private FileConfiguration config;
	private FileConfiguration sign;
	private List<ServerInfo> servers = new ArrayList<>();
	private List<BungeeSign> signs = new ArrayList<>();
	private List<Block> blocks = new ArrayList<>();
	private long cooldown;
	private long signUpdates;
	private int pingTimeout;
	private int pingInterval;

	private File config_file = new File("plugins/BungeeSigns/config.yml");
	private File sign_file = new File("plugins/BungeeSigns/signs.yml");

	public enum ConfigType {
		SETTINGS, SIGNS;
	}

	public ConfigData(BungeeSigns plugin) {
		this.plugin = plugin;
	}

	public void loadConfig() {
		config = null;
		sign = null;

		servers.clear();
		signs.clear();

		if (config_file.exists()) {
			config = YamlConfiguration.loadConfiguration(config_file);
		} else {
			plugin.saveResource("config.yml", false);
			config = YamlConfiguration.loadConfiguration(config_file);
		}

		if (sign_file.exists()) {
			sign = YamlConfiguration.loadConfiguration(sign_file);
		} else {
			plugin.saveResource("signs.yml", false);
			sign = YamlConfiguration.loadConfiguration(sign_file);
		}

		loadSettings();
		loadServers();
		loadSigns();
	}

	public void reloadConfig() {
		config = null;
		sign = null;

		servers.clear();
		signs.clear();

		loadConfig();
	}

	public void unloadConfig() {
		config = null;
		sign = null;

		servers.clear();
		signs.clear();
	}

	private void loadSettings() {
		this.log = this.config.getBoolean("options.logConsole");
		this.cooldown = (this.config.getInt("options.use-cooldown") * 1000);
		this.signUpdates = this.config.getInt("options.sign-updates");
		this.pingInterval = this.config.getInt("options.ping-interval");
		this.pingTimeout = this.config.getInt("options.ping-timeout");
	}
	
	public void addServer(ServerInfo server) {
		
		this.servers.add(server);
		
	}

	private void loadServers() {
		ConfigurationSection srv = this.config
				.getConfigurationSection("servers");

		for (String server : srv.getKeys(false)) {
			ConfigurationSection cs = srv.getConfigurationSection(server);
			String displayname = cs.getString("displayname");
			String[] address = cs.getString("address").split(":");
			String ip = address[0];
			String port = address[1];

			ServerInfo serverping = new ServerInfo(server, displayname, ip,
					Integer.valueOf(port), this.pingTimeout);
			serverping.resetPingDelay();
			this.servers.add(serverping);
		}
	}

	private void loadSigns() {
		for (String sign : this.sign.getStringList("signs")) {
			Location location = LocationSerialiser.stringToLocationSign(sign);
			String server = LocationSerialiser.getServerFromSign(sign);

			Block b = location.getBlock();
			this.blocks.add(b);

			BungeeSign serversign = new BungeeSign(server, location);
			this.signs.add(serversign);
		}
	}

	public ServerInfo getServer(String server) {
		for (ServerInfo info : this.servers) {
			if (info.getName().equals(server)) {
				return info;
			}
		}

		return null;
	}

	public long getCooldown() {
		return this.cooldown;
	}

	public void setCooldown(int seconds) {
		this.cooldown = seconds * 1000;
	}

	public boolean getConsoleLog() {
		return this.log;
	}

	public BungeeSigns getPlugin() {
		return this.plugin;
	}

	public FileConfiguration getConfig(ConfigType type) {
		if (type.equals(ConfigType.SETTINGS)) {
			return this.config;
		} else if (type.equals(ConfigType.SIGNS)) {
			return this.sign;
		}

		return null;
	}

	public void saveConfig(ConfigType type) throws IOException {
		if (type.equals(ConfigType.SETTINGS)) {
			config.save(config_file);
		} else if (type.equals(ConfigType.SIGNS)) {
			sign.save(sign_file);
		}
	}

	public List<ServerInfo> getServers() {
		return this.servers;
	}

	public void setServers(List<ServerInfo> servers) {
		this.servers = servers;
	}

	public List<BungeeSign> getSigns() {
		return this.signs;
	}

	public void setSigns(List<BungeeSign> signs) {
		this.signs = signs;
	}
	
	public List<Block> getBlocks() {
		return this.blocks;
	}

	public void setBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}

	public long getUpdateInterval() {
		return signUpdates;
	}

	public void setUpdateInterval(long signUpdates) {
		this.signUpdates = signUpdates;
	}

	public int getPingInterval() {
		return this.pingInterval;
	}

	public int getPingTimeout() {
		return this.pingTimeout;
	}

	public void setPingInterval(int interval) {
		this.pingInterval = interval;
	}

	public void setPingTimeout(int timeout) {
		this.pingTimeout = timeout;
	}

	public BungeeSign getSignFromLocation(Location l) {
		for (BungeeSign sign : signs) {
			if (l.equals(sign.getLocation())) {
				return sign;
			}
		}

		return null;
	}

	public boolean containsSign(Block b) {
		if (blocks.contains(b)) {
			return true;
		} else {
			return false;
		}
	}

	public void addSign(Block b, String server) {
		String index = LocationSerialiser.locationSignToString(b.getLocation(),
				server);
		List<String> list = this.sign.getStringList("signs");
		list.add(index);
		this.sign.set("signs", list);

		try {
			this.saveConfig(ConfigType.SIGNS);
		} catch (IOException e) {
			e.printStackTrace();
		}

		BungeeSign ssign = new BungeeSign(server, b.getLocation());
		signs.add(ssign);
		blocks.add(b);
	}

	public void removeSign(Block b) {
		Location location;
		for (BungeeSign sign : signs) {
			if (b.getLocation().equals(sign.getLocation())) {
				location = sign.getLocation();
				String index = LocationSerialiser.locationSignToString(
						location, sign.getServer());
				List<String> list = this.sign.getStringList("signs");
				list.remove(index);
				this.sign.set("signs", list);

				try {
					this.saveConfig(ConfigType.SIGNS);
				} catch (IOException e) {
					e.printStackTrace();
				}

				signs.remove(sign);
				blocks.remove(b);
				break;
			}
		}
	}
}
