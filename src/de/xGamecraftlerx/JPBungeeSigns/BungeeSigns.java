package de.xGamecraftlerx.JPBungeeSigns;

import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import de.xGamecraftlerx.JPBungeeSigns.ConfigData.ConfigType;
import de.xGamecraftlerx.JPBungeeSigns.Handlers.ServerListener;
import de.xGamecraftlerx.JPBungeeSigns.Utils.BungeeSign;
import de.xGamecraftlerx.JPBungeeSigns.Utils.ServerInfo;

public class BungeeSigns extends JavaPlugin implements PluginMessageListener {

	private static BungeeSigns instance;
	private static ConfigData data;
	private static Scheduler scheduler;

	public static String pre = "§7[§6TeleportSigns§7] §c";

	@Override
	public void onEnable() {

		// LOAD INSTANCES
		instance = this;
		data = new ConfigData(this);
		scheduler = new Scheduler(this);

		// LOAD DATABASES
		data.loadConfig();
		
		getCommand("addserver").setExecutor(this);
		getCommand("deleteserver").setExecutor(this);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				
				// START SCHEDULERS
				scheduler.startSignScheduler();
				scheduler.startPingScheduler();

				// EVENTS
				Bukkit.getPluginManager().registerEvents(
						new ServerListener(instance), instance);

				// BUNGEECORD API
				getServer().getMessenger().registerOutgoingPluginChannel(
						instance, "BungeeCord");
				getServer().getMessenger().registerIncomingPluginChannel(
						instance, "BungeeCord", instance);
                                
                                for(int i = 0; i < data.getSigns().size(); i++) {
                                    
                                    BungeeSign s = data.getSigns().get(i);
                                    
                                }
			}
		}, 5L);
	}

	@Override
	public void onDisable() {

		// RESET SIGNS
		for (int i = 0; i < data.getSigns().size(); i++) {
			BungeeSign s = data.getSigns().get(i);
			if (s.getLocation().getBlock().getState() instanceof Sign) {
				Sign sign = (Sign) s.getLocation().getBlock().getState();
				sign.setLine(0, "");
				sign.update(true);
				sign.setLine(1, "");
				sign.update(true);
				sign.setLine(2, "");
				sign.update(true);
				sign.setLine(3, "");
				sign.update(true);
			}
		}

		// UNLOAD CONFIG
		data.unloadConfig();
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		Player player = (Player) cs;
		
		if(cmd.getName().equalsIgnoreCase("addserver")) {
			
			if(player.hasPermission("jpgames.admin")) {
				
				if(args.length < 3) {
					
					player.sendMessage("§c/addserver <name> <ip> <port>");
					return true;	
					
				}
				
				if(args.length == 3) {
					
					if(cmd.getName().equalsIgnoreCase("addserver")) {
						
						String name = args[0];
						String ip = args[1];
						Integer port = 0;
						
						try {
							
							port = Integer.valueOf(args[2]);
							
							FileConfiguration cfg = data.getConfig(ConfigType.SETTINGS);
							
							if(cfg.getString("servers." + name + ".address") == null) {
								
								cfg.set("servers." + name + ".displayname", name);
								cfg.set("servers." + name + ".address", ip + ":" + port);
									
									ServerInfo serverping = new ServerInfo(name, name, ip,
											Integer.valueOf(port), 4000);
									serverping.resetPingDelay();
									
									try {
										data.saveConfig(ConfigType.SETTINGS);
									} catch (IOException e) {
										e.printStackTrace();
									}
									
									data.addServer(serverping);
								
								player.sendMessage("§aDu hast einen Server hinzugef§gt.");
								
							} else {
								
								player.sendMessage("§cDieser Server existiert bereits.");
								
							}
							
						} catch (NumberFormatException e) {
							
							player.sendMessage("§cBitte gib eine Nummer an.");
							
						}
						
					}
					
				}
				
			}
			
		}
		
		if(cmd.getName().equalsIgnoreCase("deleteserver")) {
			
			if(player.hasPermission("jp.admin")) {
				
				if(args.length == 0) {
					
					player.sendMessage("§c/deleteserver <name>");
					return true;	
					
				}
				
				if(args.length == 1) {
					
					FileConfiguration cfg = data.getConfig(ConfigType.SETTINGS);
					
					String name = args[0];
					
					if(cfg.getString("servers." + name) != null) {
						
						cfg.set("servers." + name, null);
						
						player.sendMessage(pre + "Du hast den Server " + name + " erfolgreich gel§scht.");
						
						try {
							data.saveConfig(ConfigType.SETTINGS);
						} catch (IOException e) {
							e.printStackTrace();
						}
						
					} else {
						
						player.sendMessage(pre + "Dieser Server existiert nicht.");
						
					}
					
				}
				
			}
			
		}
		return true;
		
	}

	public static BungeeSigns getInstance() {
		return instance;
	}

	public ConfigData getConfigData() {
		return data;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void logConsole(Level level, String error) {
		if (data.getConsoleLog() == true) {
			Bukkit.getLogger().log(level, error);
		}
	}

	// Bungeecord
	@Override
	public void onPluginMessageReceived(String channel, Player player,
			byte[] msg) {
		if (!channel.equals("BungeeCord")) {
			return;
		}
	}
}
