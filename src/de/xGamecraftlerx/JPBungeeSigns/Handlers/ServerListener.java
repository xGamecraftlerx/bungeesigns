package de.xGamecraftlerx.JPBungeeSigns.Handlers;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import de.xGamecraftlerx.JPBungeeSigns.BungeeSigns;
import de.xGamecraftlerx.JPBungeeSigns.Events.BungeeSignsCreateEvent;
import de.xGamecraftlerx.JPBungeeSigns.Events.BungeeSignsDestroyEvent;
import de.xGamecraftlerx.JPBungeeSigns.Events.BungeeSignsInteractEvent;
import de.xGamecraftlerx.JPBungeeSigns.Utils.BungeeSign;
import de.xGamecraftlerx.JPBungeeSigns.Utils.ServerInfo;

//ONE SECOND = 1000ms
public class ServerListener implements Listener {

	BungeeSigns plugin;

	public ServerListener(BungeeSigns plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void createBungeeSign(SignChangeEvent e) {
		if (!e.isCancelled()) {
			if (e.getLine(0).equalsIgnoreCase("[bsigns]")) {
				if (e.getPlayer().hasPermission("bungeesigns.create")) {
					ServerInfo server = BungeeSigns.getInstance()
							.getConfigData().getServer(e.getLine(1));

					if (server != null) {
						BungeeSign ssign = new BungeeSign(server.getName(), e
								.getBlock().getLocation());
						BungeeSignsCreateEvent event = new BungeeSignsCreateEvent(
								e.getPlayer(), ssign);
						Bukkit.getPluginManager().callEvent(event);

						if (!event.isCancelled()) {
							BungeeSigns.getInstance().getConfigData()
									.addSign(e.getBlock(), server.getName());
							e.getPlayer().sendMessage(
									BungeeSigns.pre
											+ "§aSchild wurde erstellt.");
						}
					} else {
						e.getPlayer().sendMessage(
								BungeeSigns.pre + "§cDer Server '"
										+ e.getLine(1) + "' existiert nicht!");
						e.getBlock().breakNaturally();
					}
				} else {
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void removeBungeeSign(BlockBreakEvent e) {
		if (!e.isCancelled()) {
			if (e.getBlock().getState() instanceof Sign) {
				if (BungeeSigns.getInstance().getConfigData()
						.containsSign(e.getBlock())) {
					if (e.getPlayer().hasPermission("bungeesigns.destroy")) {
						BungeeSign ssign = BungeeSigns
								.getInstance()
								.getConfigData()
								.getSignFromLocation(e.getBlock().getLocation());
						BungeeSignsDestroyEvent event = new BungeeSignsDestroyEvent(
								e.getPlayer(), ssign);
						Bukkit.getPluginManager().callEvent(event);

						if (!event.isCancelled()) {
							BungeeSigns.getInstance().getConfigData()
									.removeSign(e.getBlock());
							e.getPlayer().sendMessage(
									BungeeSigns.pre
											+ "§aSchild erfolgreich zerst§rt.");
						}
					} else {
						e.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void interactBungeeSign(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getState() instanceof Sign) {
				if (BungeeSigns.getInstance().getConfigData().getBlocks()
						.contains(e.getClickedBlock())) {
					for (BungeeSign ssign : BungeeSigns.getInstance()
							.getConfigData().getSigns()) {
						if (ssign != null
								&& !ssign.isBroken()
								&& ssign.getLocation().equals(
										e.getClickedBlock().getLocation())) {
							ServerInfo server = BungeeSigns.getInstance()
									.getConfigData()
									.getServer(ssign.getServer());
							if (server != null) {
								e.setCancelled(true);
								BungeeSignsInteractEvent event = new BungeeSignsInteractEvent(
										e.getPlayer(), ssign, server);
								Bukkit.getPluginManager().callEvent(event);

								if (!event.isCancelled()) {
									if (server.isOnline()) {
										ssign.teleportPlayer(e.getPlayer());
									} else {
										e.getPlayer()
												.sendMessage(
														"§cDieser Server ist nicht erreichbar.");
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
