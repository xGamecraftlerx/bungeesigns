package de.xGamecraftlerx.JPBungeeSigns.Utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import de.xGamecraftlerx.JPBungeeSigns.BungeeSigns;

public class BungeeSign {
	private String world;
	private int x;
	private int y;
	private int z;

	private String id;
	private String server;
	private boolean broken;

	public BungeeSign(String server, Location location) {
		this.world = location.getWorld().getName();
		this.x = location.getBlockX();
		this.y = location.getBlockY();
		this.z = location.getBlockZ();

		this.server = server;
		this.broken = false;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getServer() {
		return server;
	}

	public void setWorld(String world) {
		this.world = world;
	}

	public String getWorld() {
		return world;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public int getZ() {
		return z;
	}

	public Location getLocation() {
		return new Location(Bukkit.getWorld(getWorld()), getX(), getY(), getZ());
	}

	public void setLocation(Location location) {
		setWorld(location.getWorld().getName());
		setX(location.getBlockX());
		setY(location.getBlockY());
		setZ(location.getBlockZ());
	}

	public boolean isBroken() {
		return broken;
	}

	public void updateSign() {
		if (!isBroken()) {
			Location location = getLocation();

			if (location.getWorld().getChunkAt(location).isLoaded()) {
				Block b = location.getBlock();
				if (b.getState() instanceof Sign) {
					ServerInfo server = BungeeSigns.getInstance()
							.getConfigData().getServer(this.server);

					Sign sign = (Sign) b.getState();

					if (server != null) {

						if (server.getMotd() != null) {

							if (server.getMotd().equalsIgnoreCase("InGame")
									&& !server.getMotd().contains("Lobby")) {

								sign.setLine(0, ChatColor.DARK_RED + "[InGame]");
								sign.setLine(1, "" + ChatColor.translateAlternateColorCodes('&', server.getDisplayname()) + "");
								sign.setLine(2, "§7» " + server.getPlayerCount() + "/" + server.getMaxPlayers() + " §7«");
								sign.setLine(3, "§4§lZuschauen");

								sign.update(true);

							}

							if (server.getMotd().equalsIgnoreCase("Lobby")
									&& server.getPlayerCount() <= server
											.getMaxPlayers()) {

								sign.setLine(0, ChatColor.GREEN + "[Beitreten]");
								sign.setLine(1, "" + ChatColor.translateAlternateColorCodes('&', server.getDisplayname()) + "");
								sign.setLine(2, "§7» " + server.getPlayerCount() + "/" + server.getMaxPlayers() + " §7«");
								sign.setLine(3, "§a§lLobby");

								sign.update(true);

							}

							if (server.getMotd().equalsIgnoreCase("Lobby")
									&& server.getPlayerCount() == server
											.getMaxPlayers()) {

								sign.setLine(0, ChatColor.GOLD + "[Premium]");
								sign.setLine(1, "" + ChatColor.translateAlternateColorCodes('&', server.getDisplayname()) + "");
								sign.setLine(2, "§6» " + server.getPlayerCount() + "/" + server.getMaxPlayers() + " §6«");
								sign.setLine(3, "█████████");

								sign.update(true);

							}

							if (server.getMotd().equalsIgnoreCase("A Minecraft Server")) {

								sign.setLine(0, "█████████");
								sign.setLine(1, "Lädt..");
								sign.setLine(2, "");
								sign.setLine(3, "█████████");

								sign.update(true);

							}

						} else {

							signError(sign);

						}

					} else {

						signError(sign);

					}
					
					if(server == null) {

						signError(sign);
						
					}

				}
			}
		}
	}

	private void signError(Sign sign) {
		if (sign != null) {

			sign.setLine(0, "§4§l█████████");
			sign.setLine(1, "§4Neustart..");
			sign.setLine(2, "§0" + sign.getLine(1));
			sign.setLine(3, "§4§l█████████");

			sign.update(true);
		}
	}

	public void teleportPlayer(Player player) {
		// CONNECT
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);

		try {
			out.writeUTF("Connect");
			out.writeUTF(this.server);
		} catch (IOException e1) {
			BungeeSigns.getInstance().logConsole(Level.WARNING,
					player.getName() + ": You'll never see me!");
		}

		player.sendPluginMessage(BungeeSigns.getInstance(), "BungeeCord",
				b.toByteArray());
	}

}
