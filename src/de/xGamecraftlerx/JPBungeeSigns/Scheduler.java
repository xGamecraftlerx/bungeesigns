package de.xGamecraftlerx.JPBungeeSigns;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import de.xGamecraftlerx.JPBungeeSigns.Events.BungeeSignsPingEvent;
import de.xGamecraftlerx.JPBungeeSigns.Events.BungeeSignsUpdateEvent;
import de.xGamecraftlerx.JPBungeeSigns.Utils.BungeeSign;
import de.xGamecraftlerx.JPBungeeSigns.Utils.ServerInfo;

public class Scheduler implements Listener {
	private BungeeSigns plugin;
	private BukkitRunnable pingScheduler;
	private BukkitRunnable signScheduler;

	public Scheduler(BungeeSigns plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	public void startPingScheduler() {
		BukkitRunnable runnable = new BukkitRunnable() {
			@Override
			public void run() {
				for (ServerInfo server : plugin.getConfigData().getServers()) {
					BungeeSignsPingEvent event = new BungeeSignsPingEvent(
							server);
					Bukkit.getPluginManager().callEvent(event);
					if (!event.isCancelled()) {
						server.ping();
					}
				}
			}
		};

		pingScheduler = runnable;
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin,
				runnable, 100L,
				BungeeSigns.getInstance().getConfigData().getPingInterval());
	}

	@SuppressWarnings("deprecation")
	public void startSignScheduler() {
		BukkitRunnable runnable = new BukkitRunnable() {
			@Override
			public void run() {
				for (BungeeSign sign : plugin.getConfigData().getSigns()) {
					BungeeSignsUpdateEvent event = new BungeeSignsUpdateEvent(
							sign);
					Bukkit.getPluginManager().callEvent(event);
					if (!event.isCancelled()) {

						sign.updateSign();
						
					}
				}
			}
		};

		signScheduler = runnable;
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,
				runnable, 40L,
				BungeeSigns.getInstance().getConfigData().getUpdateInterval());
	}

	public BukkitRunnable getPingScheduler() {
		return this.pingScheduler;
	}

	public BukkitRunnable getSignScheduler() {
		return this.signScheduler;
	}
}
